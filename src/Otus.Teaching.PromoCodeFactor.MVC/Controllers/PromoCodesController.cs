﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactor.MVC.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactor.MVC.Controllers
{
    public class PromoCodesController : Controller
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;

        public PromoCodesController(IRepository<PromoCode> promoCodesRepository)
        {
            _promoCodesRepository = promoCodesRepository;
        }

        // GET: PromoCodes
        public async Task<IActionResult> Index()
        {
            IEnumerable<PromoCode> promoCodes;
            try
            {
                promoCodes = await _promoCodesRepository.GetAllAsync();
                if (promoCodes == null) new List<PromoCode>();
            }
            catch (Exception)
            {
                promoCodes = new List<PromoCode>();
            }

            var response = promoCodes.Select(x => new PromocodeViewModel(x)).ToList();

            return View(response);
        }

        // GET: PromoCodes/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promoCode = await _promoCodesRepository.GetByIdAsync(id.Value);

            if (promoCode == null)
            {
                return NotFound();
            }

            var response = new PromocodeFullViewModel(promoCode);
            return View(response);
        }

        // GET: PromoCodes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PromoCodes/Create       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PromocodeViewModel request)
        {
            if (!ModelState.IsValid) return View(request);

            var promoCode = new PromoCode()
            {
                ServiceInfo = request.ServiceInfo,
                BeginDate = request.BeginDate,
                Code = request.Code,
                EndDate = request.EndDate,
                PartnerManagerId = request.PartnerManager,
                PartnerName = request.PartnerName,
                PreferenceId = request.Preference
            };


            await _promoCodesRepository.AddAsync(promoCode);

            return RedirectToAction(nameof(Index));
        }

        // GET: PromoCodes/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promoCode = await _promoCodesRepository.GetByIdAsync(id.Value);

            if (promoCode == null)
            {
                return NotFound();
            }

            return View(new PromocodeViewModel(promoCode));
        }

        // POST: PromoCodes/Edit/5        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PromocodeViewModel request)
        {
            if (!ModelState.IsValid) return View(request);

            var promoCode = await _promoCodesRepository.GetByIdAsync(request.Id);

            if (promoCode == null)
                return NotFound();

            promoCode.ServiceInfo = request.ServiceInfo;
            promoCode.BeginDate = request.BeginDate;
            promoCode.Code = request.Code;
            promoCode.EndDate = request.EndDate;
            promoCode.PartnerManagerId = request.PartnerManager;
            promoCode.PartnerName = request.PartnerName;
            promoCode.PreferenceId = request.Preference;

            await _promoCodesRepository.UpdateAsync(promoCode);

            return RedirectToAction(nameof(Index));
        }

        // GET: PromoCodes/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promoCode = await _promoCodesRepository.GetByIdAsync(id.Value);

            if (promoCode == null)
            {
                return NotFound();
            }

            return View(new PromocodeViewModel(promoCode));
        }

        // POST: PromoCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);

            if (promoCode == null)
                return NotFound();

            await _promoCodesRepository.DeleteAsync(promoCode);

            return RedirectToAction(nameof(Index));
        }

        private bool PromoCodeExists(Guid id)
        {
            var promoCode = _promoCodesRepository.GetByIdAsync(id).Result;

            return (promoCode != null);
        }
    }
}