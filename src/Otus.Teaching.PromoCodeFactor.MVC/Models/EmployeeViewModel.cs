﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactor.MVC.Models
{
    public class EmployeeViewModel
    {
        [Display(Name = "Имя")] public string FirstName { get; set; }

        [Display(Name = "Фамилия")] public string LastName { get; set; }

        [Display(Name = "Адрес")] public string Email { get; set; }

        [Display(Name = "Роль")] public string Role { get; set; }

        [Display(Name = "Полное имя")]
        public string FullName
        {
            get => $"{Role}: {FirstName} {LastName}";
        }
    }
}