﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactor.MVC.Models
{
    public class PartnerFullViewModel
    {
        public PartnerFullViewModel()
        {
        }

        public PartnerFullViewModel(Partner partner)
        {
            Id = partner.Id;
            Name = partner.Name;
            NumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;
            IsActive = partner.IsActive;
            PartnerLimits = new List<PartnerPromoCodeLimitViewModel>();
            foreach (var item in partner.PartnerLimits)
            {
                PartnerLimits.Add(
                    new PartnerPromoCodeLimitViewModel()
                    {
                        CreateDate = item.CreateDate,
                        CancelDate = item.CancelDate,
                        EndDate = item.EndDate,
                        Limit = item.Limit
                    }
                );
            }
        }

        [Display(Name = "Идентификационный номер")]
        public Guid Id { get; set; }

        [Display(Name = "Имя")] public string Name { get; set; }

        [Display(Name = "Количество выданных промокодов")]
        public int NumberIssuedPromoCodes { get; set; }

        [Display(Name = "Активен")] public bool IsActive { get; set; }

        [Display(Name = "Ограничения партнера")]
        public List<PartnerPromoCodeLimitViewModel> PartnerLimits { get; set; }
    }
}