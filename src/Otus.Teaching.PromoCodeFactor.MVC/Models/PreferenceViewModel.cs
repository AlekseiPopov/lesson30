﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactor.MVC.Models
{
    public class PreferenceViewModel
    {
        public PreferenceViewModel()
        {
        }

        public PreferenceViewModel(Preference preference)
        {
            Id = preference.Id;
            Name = preference.Name;
        }

        [Display(Name = "Идентификационный номер")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Поле '{0}' должно быть заполнено")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        [Display(Name = "Название")]
        public string Name { get; set; }
    }
}