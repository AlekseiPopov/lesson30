﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactor.MVC.Models
{
    public class PromocodeFullViewModel
    {
        public PromocodeFullViewModel()
        {
        }

        public PromocodeFullViewModel(PromoCode promoCode)
        {
            Id = promoCode.Id;
            Code = promoCode.Code;
            ServiceInfo = promoCode.ServiceInfo;
            BeginDate = promoCode.BeginDate;
            EndDate = promoCode.EndDate;
            PartnerName = promoCode.PartnerName;
            Preference = promoCode.Preference.Name;
            PartnerManager = new EmployeeViewModel()
            {
                FirstName = promoCode.PartnerManager.FirstName,
                LastName = promoCode.PartnerManager.FirstName,
                Email = promoCode.PartnerManager.Email,
                Role = promoCode.PartnerManager.Role.Name
            };
        }


        [Display(Name = "Идентификационный номер")]
        public Guid Id { get; set; }

        [Display(Name = "Промокод")] public string Code { get; set; }

        [Display(Name = "Информация")] public string ServiceInfo { get; set; }

        [Display(Name = "Дата создания")] public DateTime BeginDate { get; set; }

        [Display(Name = "Дата окончания")] public DateTime EndDate { get; set; }

        [Display(Name = "Имя партнера")] public string PartnerName { get; set; }

        [Display(Name = "Менеджер")] public EmployeeViewModel PartnerManager { get; set; }

        [Display(Name = "Предпочтение")] public string Preference { get; set; }
    }
}