﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Dto;

namespace Otus.Teaching.PromoCodeFactory.Core.Interfaces.Services
{
    public interface IPartnerService
    {
        Task<List<PartnerResponse>> GetPartnersAsync();

        Task<PartnerResponse> GetPartnerByIdAsync(Guid id);

        Task<PartnerPromoCodeLimitResponse> GetPartnerLimitAsync(Guid id, Guid limitId);

        Task<Guid> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest requestDto);

        Task CancelPartnerPromoCodeLimitAsync(Guid id);
    }
}